%% geodesic interpolation for general K
%
% Computes discrete geodesic with K+1 points
%
% Input
%   qA, qB, K
%       Boundary conditions for discrete geodesic
%   splineData
%       General information about the splines used.
%
% Output
%   q
%       Discrete geodesic path of length 3
%
function qAll = discreteGeodesic(qA, qB, K, splineData, varargin)

% Some useful constants
N = splineData.N;
dSpace = splineData.dSpace;

options = optimset('TolFun', 1e-6, ...
                   'Display', 'off');
%options = optimoptions('fsolve');
%options = optimoptions(options,'TolFun', 1e-6);
%options = optimoptions(options,'Display','off');
%options = optimoptions(options,'MaxIter',400);
%options = optimoptions(options,'MaxFunEvals',10000);

% Here we save the geodesic
qAll = zeros(N, dSpace, K+1);
qAll(:,:,1) = qA;
qAll(:,:,K+1) = qB;

% linear interpolation as initialization
q_init = zeros( (K-1)*N, dSpace );
tau = 1/K;
vel = (qB - qA)/K;
for k = 1:(K-1)
  q_init((k-1)*N+1:k*N, : ) = qA + k*vel;
end


%F = @(q) LagrangianLeftDer( q, q1, q0, splineData );
F = @(q) LagrangianLeftDerAll( qA, qB, q, K, splineData );
[qShapes, ~, ~] = fsolve( F, q_init, options);

%% cast back 
for k = 1:(K-1)
  qAll(:,:,k+1) = qShapes((k-1)*N+1:k*N, : );
end

end



% E(d0, d1, d2) = G_{d0}(d1-d0, d1-d0) + G_{d1}(d2-d1, d2-d2)
% D_{d1} E(...) (h) = 2 * G_{d0}(d1-d0, h) - 2 * G_{d1}(d2-d1, h)
%                      + D_{d1} G_{}(d2-d1,d2-d1) (h)
function Eder = LagrangianLeftDer(d2, d1, d0, splineData)
   [~, ~, dG] = curveRiemH2InnerProd(d1, d2-d1, d2-d1, splineData);
   
   Eder = 2 * curveRiemH2Flat(d0, d1-d0, splineData) ...
            - 2 * curveRiemH2Flat(d1, d2-d1, splineData) ...
            + dG;
end

% E(d0, d1, d2) = G_{d0}(d1-d0, d1-d0) + G_{d1}(d2-d1, d2-d2)
% D_{d1} E(...) (h) = 2 * G_{d0}(d1-d0, h) - 2 * G_{d1}(d2-d1, h)
%                      + D_{d1} G_{}(d2-d1,d2-d1) (h)
function Eder = LagrangianLeftDerAll(dA, dB, dAll, K, splineData)
   N = length(dA);
   dSpace = splineData.dSpace;
   totalNumFreePts = N * (K-1);
   assert( length(dAll) == totalNumFreePts );   
   Eder = zeros( totalNumFreePts, dSpace );
 
   % concatenate shapes
   allShapes = zeros( N * (K+1), dSpace);
   allShapes(1:N, : ) = dA;
   allShapes((N+1):(N+totalNumFreePts), : ) = dAll;
   allShapes( (N*K+1):N*(K+1), : ) = dB;
   
   for k = 2:K
     idx0 = (k-2)*N;
     idx1 = (k-1)*N;
     idx2 = k*N;
     idx3 = (k+1)*N;
     
     d0 = allShapes(idx0+1:idx1,:);
     d1 = allShapes(idx1+1:idx2,:);
     d2 = allShapes(idx2+1:idx3,:);
     
     [~, ~, dG] = curveRiemH2InnerProd(d1, d2-d1, d2-d1, splineData);
   
     Eder((k-2)*N+1:(k-1)*N, : ) = 2 * curveRiemH2Flat(d0, d1-d0, splineData)  - 2 * curveRiemH2Flat(d1, d2-d1, splineData)  + dG;     
   end

end