%% discrete geodesic interpolation for K=2
%
% Computes discrete 3 point geodesic (K=2) an returns intermediate point
%
% Input
%   q0, q2
%       Boundary conditions for discrete geodesic
%   splineData
%       General information about the splines used.
%
% Output
%   q1
%       midpoint such that (q0, q1, q2) is discrete geodesic of length 3
%
function q1 = getGeodMidPoint(q0, q2, splineData, varargin)

geodPath = discrete3PtGeodesic(q0, q2, splineData, varargin);
N = splineData.N;
q1 = geodPath(N+1:2*N,:);

end
