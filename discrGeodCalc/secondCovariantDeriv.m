%% First order consistent approximation of second covariant derivative
%
% Computes approximation of \nabla_v \nabla_u \eta(y)
%
% Input
%   y, v, u, eta, tau
%       Evaluation point y, tangent vector eta, outer/inner direction v and u, respectively, scaling parameter tau
%   splineData
%       General information about the splines used.
%
% Output
%   \nabla_v \nabla_u \eta(y)
%       By construction of symmetrized, reflected and scaled Schild's ladder construction
%       Scaling of inner direction u via tau^(3/2), scaling of outer direction v via tau.
%
function secCovDer = secondCovariantDeriv(y, outerDir, innerDir, eta, tau, splineData, varargin)  

  % compute scaled offsets of evaluation point y
  yRight = y + tau * outerDir;
  yLeft  = y - tau * outerDir;
  
  % compute two inner first covariant derivatives
  innerTau = tau * sqrt(tau);
  firstDerRight = firstCovariantDeriv( yRight, innerDir, eta, innerTau, innerTau, splineData, varargin );
  firstDerLeft  = firstCovariantDeriv( yLeft, innerDir, eta, innerTau, innerTau, splineData, varargin );
  
  % compute scaled points from tangent vectors
  firstDerRightPt = yRight + tau * firstDerRight;
  firstDerLeftPt  = yLeft  - tau * firstDerLeft; % NOTE the sign here!
  
  % now compute outer first covariant derivative (of inner cov. deriv.) by hand
  secDerRightPt = backwardTranspSingleStep(y, yRight, firstDerRightPt, splineData, varargin );
  secDerLeftPt  = backwardTranspSingleStep(y, yLeft,  firstDerLeftPt,  splineData, varargin );
  
  % compute tangent vectors at evaluation point y
  secDerRight = secDerRightPt - y; 
  secDerLeft  = secDerLeftPt - y;
  
  % compute sum (!!) of tangent vectors due to change in sign in transported vector (see above)
  secCovDer = (secDerRight + secDerLeft) / (2 * tau * tau);
end
