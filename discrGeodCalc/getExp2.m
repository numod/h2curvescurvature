%% discrete geodesic extrapolation for K=2
%
% Computes discrete Exp2 shape
%
% Input
%   q0, q1
%       Initial conditions for discrete geodesic shooting
%   splineData
%       General information about the splines used.
%
% Output
%   q2
%       Discrete Exp2 such that (q0, q1, q2) is discrete geodesic of length 3
%
function q2 = getExp2(q0, q1, splineData, varargin)
  shootedPath = geodesicForward(q0, q1, 2, splineData);
  q2 = shootedPath(:,:,3);
end
