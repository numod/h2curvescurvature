%% Evaluation of Schild's ladder
%
% Single step of FORWARD discrete parallel transport
%
% Input
%   q0, q1, p0
%       Discrete points q0 and q1 and offset p0 (here zeta0 = p0-q0 is the vector to be transported)
%   splineData
%       General information about the splines used.
%
% Output
%   p1
%       p1 = q1 + zeta1, where zeta1 is the parallely transported vector sitting at q1
%
function p1 = forwardTranspSingleStep(q0, q1, p0, splineData, varargin)
  % compute center point c such that (p0, c, q1) is discrete geodesic
  center = getGeodMidPoint(p0, q1, splineData, varargin);
  % compute p1 = q1 + zeta1 such that (q0, c, p1) is discrete geodesic
  p1 = getExp2(q0, center, splineData, varargin);
end
