%% Second order consistent approximation of first covariant derivative
%
% Computes approximation of \nabla_u \eta(y)
%
% Input
%   y, v, eta, tau, eps
%       Evaluation point y, tangent vectors u and eta, scaling parameters tau and eps
%   splineData
%       General information about the splines used.
%
% Output
%   \nabla_u \eta(y)
%       By construction of symmetrized, reflected and scaled Schild's ladder construction
%       Scaling of u via tau, scaling of eta via eps (usually tau = eps)
%
function firCovDer = firstCovariantDeriv(y, u, eta, tau, eps, splineData, varargin)
  % compute scaled offsets of evaluation point y
  yRight = y + tau * u;
  yLeft  = y - tau * u;
  
  % compute scaled and shifted tangent vectors
  yURightEta = yRight + eps * eta;
  yULeftEta  = yLeft - eps * eta; %NOTE the sign here, this is where the proper reflection happens!
  
  % transport back to y by inverse parallel transport (NOTE: results are shapes here!)
  rightEtaPt = backwardTranspSingleStep(y, yRight, yURightEta, splineData, varargin);
  leftEtaPt  = backwardTranspSingleStep(y, yLeft,  yULeftEta, splineData, varargin);
  
  % compute tangent vectors at evaluation point y
  rightEta = rightEtaPt - y; 
  leftEta  = leftEtaPt - y;
  
  % compute sum (!!) of tangent vectors due to change in sign in transported vector (see above)
  firCovDer = (rightEta + leftEta ) / (2 * tau * eps);
end
