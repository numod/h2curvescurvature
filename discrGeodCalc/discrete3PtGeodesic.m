%% geodesic interpolation for K=2
%
% Computes discrete 3 point geodesic (K=2)
%
% Input
%   q0, q1
%       Boundary conditions for discrete geodesic
%   splineData
%       General information about the splines used.
%
% Output
%   q
%       Discrete geodesic path of length 3
%
function qAll = discrete3PtGeodesic(q0, q2, splineData, varargin)

% Some useful constants
N = splineData.N;
dSpace = splineData.dSpace;

numDofs = length(q0);
maxIters = 100 * numDofs;
eps = 1e-10;
options = optimset('TolFun', 1e-6, 'Display', 'off', 'MaxIter', maxIters, 'TolX', eps, 'TolFun', eps );
if numDofs > 300
  disp('3pt geodesic')
  options = optimset('TolFun', 1e-6, 'Display', 'final-detailed', 'MaxIter', maxIters, 'TolX', eps, 'TolFun', eps );
end

%options = optimset('TolFun', 1e-6, 'Display', 'off');
%options = optimoptions('fsolve');
%options = optimoptions(options,'TolFun', 1e-6);
%options = optimoptions(options,'Display','off');
%options = optimoptions(options,'MaxIter',400);
%options = optimoptions(options,'MaxFunEvals',10000);

q1_init = (q0 + q2)/2.;

F = @(q) EulerLagrange3PtGeodesic( q2, q, q0, splineData );
[q1, ~, ~] = fsolve( F, q1_init, options);
    
% Here we save the geodesic
qAll = zeros(3*N, dSpace);
qAll(1:N,:) = q0;
qAll((N+1):2*N,:) = q1;
qAll((2*N+1):3*N,:) = q2;    

end


% E(d0, d1, d2) = G_{d0}(d1-d0, d1-d0) + G_{d1}(d2-d1, d2-d2)
% D_{d1} E(...) (h) = 2 * G_{d0}(d1-d0, h) - 2 * G_{d1}(d2-d1, h) + D_{d1} G_{}(d2-d1,d2-d1) (h)
function Eder = EulerLagrange3PtGeodesic(d2, d1, d0, splineData)
   [~, ~, dG] = curveRiemH2InnerProd(d1, d2-d1, d2-d1, splineData);   
   Eder = 2 * curveRiemH2Flat(d0, d1-d0, splineData) - 2 * curveRiemH2Flat(d1, d2-d1, splineData) + dG;
end