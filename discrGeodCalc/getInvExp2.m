%% inverse discrete geodesic extrapolation for K=2
%
% Computes inverse discrete Exp2 shape
%
% Input
%   q1, q2
%       Initial conditions for discrete geodesic shooting
%   splineData
%       General information about the splines used.
%
% Output
%   q0
%       Inverse discrete Exp2 such that (q0, q1, q2) is discrete geodesic of length 3
%
function q0 = getInvExp2(q1, q2, splineData, varargin)

% Some useful constants
N = splineData.N;
dSpace = splineData.dSpace;

numDofs = length(q1);
maxIters = 100 * numDofs;
eps = 1e-10;
options = optimset('TolFun', 1e-6, 'Display', 'off', 'MaxIter', maxIters, 'TolX', eps, 'TolFun', eps );
%options = optimset('TolFun', 1e-6, 'Display', 'off');
if numDofs > 300
  disp('inverse Exp2')
  options = optimset('TolFun', 1e-6, 'Display', 'final-detailed', 'MaxIter', maxIters, 'TolX', eps, 'TolFun', eps );
end

%options = optimoptions('fsolve');
%options = optimoptions(options,'TolFun', 1e-6);
%options = optimoptions(options,'Display','off');
%options = optimoptions(options,'MaxIter',400);
%options = optimoptions(options,'MaxFunEvals',10000);

q0_init = q1 + (q1-q2);

%F = @(q) LagrangianLeftDer( q, q1, q0, splineData );
F = @(q) LagrangianLeftDer( q2, q1, q, splineData );
[q0, ~, ~] = fsolve( F, q0_init, options);

end

% E(d0, d1, d2) = G_{d0}(d1-d0, d1-d0) + G_{d1}(d2-d1, d2-d2)
% D_{d1} E(...) (h) = 2 * G_{d0}(d1-d0, h) - 2 * G_{d1}(d2-d1, h)
%                      + D_{d1} G_{}(d2-d1,d2-d1) (h)
function Eder = LagrangianLeftDer(d2, d1, d0, splineData)
   [~, ~, dG] = curveRiemH2InnerProd(d1, d2-d1, d2-d1, splineData);
   
   Eder = 2 * curveRiemH2Flat(d0, d1-d0, splineData) ...
            - 2 * curveRiemH2Flat(d1, d2-d1, splineData) ...
            + dG;
end