%% Evaluation of Schild's ladder (backwards!)
%
% Single step of BACKWARD discrete parallel transport
%
% Input
%   q0, q1, p1
%       Discrete points q0 and q1 and offset p1
%   splineData
%       General information about the splines used.
%
% Output
%   p0
%       Point p0 such that zeta0 = p0-q0 being parallely transported from q0 to q1 would result in zeta1 = p1-q1
%
function p0 = backwardTranspSingleStep(q0, q1, p1, splineData, varargin)
  % compute center point c such that (q0, c, p1) is discrete geodesic
  center = getGeodMidPoint(q0, p1, splineData, varargin);
  % compute p0 = q0 + zeta0 such that (p0, c, q1) is discrete geodesic
  p0 = getInvExp2(center, q1, splineData, varargin);
end
