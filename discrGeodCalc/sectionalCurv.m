%% Second order consistent approximation of sectional curvature
%
% Computes approximation of \kappa_y( u, v )
%
% Input
%   y, v, u, tau
%       Evaluation point y, tangent vectors u and v, approximation parameter tau
%   splineData
%       General information about the splines used.
%
% Output
%   \kappa_y( u, v ) = \frac{g(u,R(u,v)v)}{g(u,u)g(v,v)-g(u,v)^2} 
%       Here, R(u,v)eta = \nabla_u \nabla_v \eta - \nabla_v \nabla_u \eta denote the Riemann curvature tensor
%       with is approximated by second order consistent second covariant derivatives
%
function secCurv = sectionalCurv(y, u, v, tau, splineData, varargin)  

  % compute Riemann curvature tensor
  secCovDeriv1 = secondCovariantDeriv(y, u, v, v, tau, splineData);
  secCovDeriv2 = secondCovariantDeriv(y, v, u, v, tau, splineData);
  Ruvv = secCovDeriv1 - secCovDeriv2;
  
  % compute sectional curvature 
  guRuvv = curveRiemH2InnerProd(y, u, Ruvv, splineData);
  guu    = curveRiemH2InnerProd(y, u, u, splineData);
  gvv    = curveRiemH2InnerProd(y, v, v, splineData);
  guv    = curveRiemH2InnerProd(y, u, v, splineData);
  secCurv = guRuvv / (guu*gvv - guv*guv);
  
end
