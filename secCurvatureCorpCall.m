%% This source contains the code for the corpus callosum example shown in the paper

clear all;
%% Set paths. Set current directory to this folder.
addpath(genpath('../source'));
addpath('../lib/varifolds');
addpath('../lib/hanso2_2');
addpath('../lib/export_fig');

%% Setup parameters for curve discretization
splineData = constructSplineData;
splineData.N = 80; % Number of spline controll points for spatial discretization
splineData.nS = 2; % Quadratic splines are needed for second order metrics  
splineData.Nt = 3; % Number of spline controll points for time discretization 
splineData.curveClosed=1; %Closed Curves (set to 0 for open curves)
splineData = finishSetup(splineData);
splineData.varData.noPts = 100; % Number of points for similarity measure (varifold) evaulation
splineData.varData.kernelSizeGeom = 0.1;
splineData.varData.kernelSizeGrass = 0.3;
splineData.varData.pts = [];
splineData = finishSetup( splineData );

% set metric parameters
splineData.a=[1 1 1 0 0]; %% a(1) = L2, a(2) = H1,  a(3) = H2, a(4) = H1v, a(5) = H1n;
splineData.scaleInv=0;         %length weighted metric (set to zero for constant coeff. metrics)
splineData.options.optDiff=1;  %Minimize over reparamtrizations of the target curve
splineData.options.optScal=0;  %Minimize over scalings of the target curve
splineData.options.optRot=1;   %Minimize over rotations of the target curve
splineData.options.optTra=1;   %Minimize over translations of the target curve
splineData.options.useAugmentedLagrangian = false; %Use quadratic penalty term 
splineData.options.varLambda = 100; %Weight of the similarity measure

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% LOAD AND PREPROCESS DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% numbers with approx. consistent parametrizations (since we consider parametrized curves!)
numbers = {'0003', '0016', '0022', '0023', '0028', '0031', '0035', '0041', '0042', '0046', '0053', '0056', '0060', '0066', '0067', '0073', '0082', '0084', '0094', '0098', '0115', '0120', '0122', '0123', '0124'};
loadstem = './data/OAS1_';
ending = '.txt';
numData = 25;

clf

% load data
dList = cell(1, numData);
for n = 1:numData
        loadname = strcat( loadstem, numbers{n} );
        %loadname = strcat( loadPath, num2str(n) );
        disp(['Process shape ', loadname])
        c = importdata( strcat(loadname, ending) );
        d = constructSplineApproximation(c(2:end,:),splineData); 
        dList{n} = 2*pi* d/curveLength(d,splineData);
end

%% alignment of all shapes wrt. the first one
disp('Start successive alignment.')
dListAligned = cell(1, numData);
dListAligned{1} = dList{1};
for n = 2:numData
        %disp(['Process shape ', num2str(n), ' of ', num2str(numData)])
        temp = rigidAlignmentVarifold({dList{1},dList{n}},splineData); 
        dListAligned{n} = 2*pi* temp{2}/curveLength(temp{2},splineData);
end

% plot input data
disp('Plot input data...')
plotCurve(dListAligned, splineData) %Plot the data
saveas(gcf, './ccExample_inputData.png')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% KARCHER MEAN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

karcherOptions.karcherMaxIter = 20;
disp(['Compute Karcher mean with ', num2str(karcherOptions.karcherMaxIter), ' iterations.'])
dMean = karcherMeanManopt(dListAligned, splineData,'options',karcherOptions);

% Plot mean in red and curves
plotCurve(dMean, splineData,'lineStyle', 'r-', 'lineWidth', 3)
saveas(gcf, './ccExample_inputData_mean.png')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PCA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

numDomPCs = 10; % number of dominant modes to be considered
disp(['Compute PCA ', num2str(numDomPCs), ' dominant modes.'])

% compute PCA
[U,Lambda,G, vList] = tangentPCA(dListAligned,dMean,splineData);  
% eigenvalues
Lambda(1:numDomPCs)
  
% plot principal components  
clf
plotCurve(dMean, splineData,'lineStyle', 'r-', 'lineWidth', 3)
  
pcList = {};  
for i = 1:numDomPCs
    N = splineData.N;
    pc = [U(1:N,i), U((N+1):2*N,i)];
    pcList{i} = pc;
    pcMean = dMean + 0.1*pc;
    plotCurve(pcMean, splineData,'lineStyle', 'k-', 'lineWidth', 1)
end
saveas(gcf, './ccExample_inputData_mean_pca.png')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SECTIONAL CURVATURES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Start to compute sectional curvature for all pairs of principal modes')
Tau = 0.004;
secCurvs = zeros(numDomPCs,numDomPCs);

for i = 1:numDomPCs
    disp(['Start to compute sectional curvature for ', num2str(numDomPCs), ' directions of shape ', num2str(i)])
    u = 0.1 * pcList{i};
    for j = (i+1):numDomPCs
        v = 0.1 * pcList{j};
        disp(['Start to compute sectional curvature for of shape ', num2str(j)])

        secCurvs(i,j) = sectionalCurv(dMean, u, v, Tau, splineData);
        disp(['Sectional curvature for tau = ', num2str(Tau), ' is ', num2str(secCurvs(i,j))])
        
        secCurvs(j,i) = sectionalCurv(dMean, v, u, Tau, splineData);
        disp(['Sectional curvature for tau = ', num2str(Tau), ' is ', num2str(secCurvs(i,j))])
    end
end

writematrix(secCurvs,'./ccExample_secCurvs.txt','Delimiter',' ')
