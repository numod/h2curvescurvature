h2curvescurvature
=========

This MATLAB code provides tools to compute approximations of the Riemann curvature tensor and sectional curvatures in the space of parametrized curves. 
The approximation is based on a variational time discretization of geodesic curves, which induces a consistent discretization of further geometric operators as the exponential map, parallel transport, covariant derivatives and finally curvature quantities. 

For details we refer to our paper

    @misc{EfHeRuWi2020,
      author  = {Alexander Effland, Behrend Heeren, Martin Rumpf, Benedikt Wirth},
      title   = {Consistent Curvature Approximation on Riemannian Shape Spaces},
      note    = {Preprint available at arXiv:1912.07336},
      year    = {2020},
    }


Please cite our paper in your work.

Dependencies
------------

The usage of our code requires the presence of the MATLAB code **h2metrics** (https://github.com/h2metrics/h2metrics.git) written by Martin Bauer, Martins Bruveris, Nicolas Charon and Jakob Moller-Andersen. Their original **h2metrics** code itself has the following dependencies:

* MATLAB Curve Fitting Toolbox
* MATLAB Optimization Toolbox

and incorporates the following libraries

* Manopt library (www.manopt.org)
* Hanso (https://cs.nyu.edu/overton/software/hanso/)
* export_fig (https://github.com/altmany/export_fig)
* fshapesTk (https://github.com/fshapes/fshapesTk)

The entire code package (**h2metrics** + **h2curvescurvature**) was tested on MATLAB R2020a.

Usage
-----

First, you have to download the code package **h2metrics** from https://github.com/h2metrics/h2metrics.git. 

Make sure **h2metrics** is working on its own by running their example m-file. (You might have to install additional libraries such as manopt, see above.)

Afterwards, clone this repo and move the folder *discrGeodCalc* into the */source* directory of **h2metrics**. 

Move the two example m-files "secCurvatureCircle.m" and "secCurvatureCorpCall.m", respectively, into the */example* directory of **h2metrics**.

Now you can run the two example files to reproduce the experiments shown in the paper.

Licence
-------

This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program. If not, see http://www.gnu.org/licenses/.

Contacts
--------

* Behrend Heeren (behrend dot heeren at ins dot uni-bonn dot de)
* Benedikt Wirth (benedikt dot wirth at uni-muenster dot de)

