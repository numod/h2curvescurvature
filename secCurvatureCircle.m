%% This source contains the convergence analysis for the sphere example shown in the paper
clear all;
%% Set paths. Set current directory to this folder.
addpath(genpath('../source'));
addpath('../lib/varifolds');
addpath('../lib/hanso2_2');
addpath('../lib/export_fig');

%%% spatial parameters
numCtrlPoints = 80;
disp(['Number of ctrl points in spline approx = ', num2str(numCtrlPoints)])
resData = 1000; 
disp(['Resolution of data (to be approximated by splines) = ', num2str(resData)])

%% Setup parameters for curve discretization
splineData = constructSplineData;
splineData.N = numCtrlPoints; % Number of spline controll points for spatial discretization
splineData.nS = 2; % Quadratic splines are needed for second order metrics  
splineData.Nt = 3; % Number of spline controll points for time discretization 
splineData.curveClosed=1; %Closed Curves (set to 0 for open curves)
splineData = finishSetup(splineData);
splineData.varData.noPts = 100; % Number of points for similarity measure (varifold) evaulation
splineData.varData.kernelSizeGeom = 0.1;
splineData.varData.kernelSizeGrass = 0.3;
splineData.varData.pts = [];
splineData = finishSetup( splineData );

%% construct circle data
c = zeros(resData,2);
for n = 1:resData
      t = 2*pi*(n-1)/resData;
      c(n,1) = cos(t);
      c(n,2) = sin(t);
end
    
% construct smooth global perturbation data
beta1 = 0.1; 
beta2 = 0.2; 
alpha1 = 0.1;  
alpha2 = 0.05; 
u = zeros(resData,2);
v = zeros(resData,2);
for n = 1:resData
      t = 2*pi*(n-1)/resData;

      % normal variation
      u(n,1) = (1 + alpha1 * sin(t/beta1) ) * cos(t);
      u(n,2) = (1 + alpha1 * sin(t/beta1) ) * sin(t);;
      
      % normal variation
      v(n,1) = (1 + alpha2 * sin(t/beta2) ) * cos(t);
      v(n,2) = (1 + alpha2 * sin(t/beta2) ) * sin(t);;
end  
    
%% approximate data by splines
d0= constructSplineApproximation(c,splineData); 
d1= constructSplineApproximation(u,splineData);
d2= constructSplineApproximation(v,splineData);
    
%Plot splines
clf
plotCurve(d0, splineData, 'lineStyle', 'k-');
hold on;
plotCurve(d1, splineData, 'lineStyle', 'g-');
hold on;
plotCurve(d2, splineData, 'lineStyle', 'b-');    
hold off;
pbaspect([1 1 1])
%legend({'y', 'u', 'v'},'Location','southwest')
title('Smooth data for convergence analysis')
saveas(gcf, './sphereExample_inputData.png')

% compute variations as differences
var1 = d1 - d0;
var2 = d2 - d0;
    
% set metric parameters
splineData.a=[1 1 1 0 0]; %% a(1) = L2, a(2) = H1,  a(3) = H2, a(4) = H1v, a(5) = H1n;

% the following parameters do not apply for the space of parametrized curves considered here!
splineData.scaleInv=0;         %length weighted metric (set to zero for constant coeff. metrics)
splineData.options.optDiff=1;  %Minimize over reparamtrizations of the target curve
splineData.options.optScal=0;  %Minimize over scalings of the target curve
splineData.options.optRot=1;   %Minimize over rotations of the target curve
splineData.options.optTra=1;   %Minimize over translations of the target curve
splineData.options.useAugmentedLagrangian = false; %Use quadratic penalty term 
splineData.options.varLambda = 100; %Weight of the similarity measure
        
% compute sectional curvature for several timesteps tau        
numTaus = 11;
startTau = 1.0;
secCurvs = zeros(numTaus,1);
taus = zeros(numTaus, 1);

Tau = startTau;
disp('Start to compute sectional curvature for different tau values.')
for n = 1:numTaus
    disp('-------------------------')
    disp(['Tau ', num2str(n), ' of ', num2str(numTaus), ' has value = ', num2str(Tau)])
    secCurvs(n) = sectionalCurv(d0, var1, var2, Tau, splineData);
    disp(['Sectional curvature is ', num2str(secCurvs(n))])
    taus(n) = Tau;
    Tau = Tau / 2;
end

% error/convergence analysis
if numTaus > 3
    disp('-------------------------')
    disp('Perform convergence analysis.')
    groundTruth = secCurvs(numTaus);
    errors = zeros(numTaus-1, 1);
    for n = 1:numTaus-1
      errors(n) = abs( (secCurvs(n) - groundTruth) / groundTruth );
    end

    %close all;
    loglog(taus(1:numTaus-1), errors, 'ro')
    hold on;

    x = logspace(-3,0);
    loglog(x,(x*10).^2, 'k-')

    xlabel('time step in covariant deriv (\tau)')
    ylabel('absolute error wrt. to ground truth')
    legend({'real error values','perfect quadratic conv.'},'Location','northwest')
    title('Second order consistency of sectional curvature')

    hold off;
    saveas(gcf, './sphereExample_secCurvConvergence.png')
end



